import sqlite3
from sqlite3 import Error
import os


class Connection:
    """
        Classe servant à se connecter à une base de données sqlite.
        @param (string) name : nom de la base de données
        @instance_variable (object) connection : objet conection retourné par sqlite3.connect(<file_path>)
        @instance_variable (object) cursor : curseur pour la db
        @instance_variable (string) directory : le chemin absolu du dossier de la bd
        @instance_variable (string) file_name : le nom du fichier de la bd
    """

    def __init__(self, name):
        self.connection = None
        self.cursor = None
        self.directory = os.path.join(os.path.dirname(__file__), "db")
        self.file_name = name + '.bd'
        self.createDBdirectory()
        self.connectDB()

    def createDBdirectory(self):
        '''
            Cette méthode créé le dossier dans lequel sera logé la base de donnés.
        '''
        if os.path.isdir(self.directory) == False:
            os.mkdir(self.path)
            # print("dossier db créé")

    def connectDB(self):
        '''
            Cette méthode se connecte à une base de données.
        '''
        if self.connection:
            self.connection.close()
        try:
            file_path = os.path.join(self.directory, self.file_name)
            self.connection = sqlite3.connect(file_path)
            self.cursor = self.connection.cursor()
        except Error as e:
            print("ERROR Connection.connectDB() : ", e)
            self.connection = None
            self.cursor = None

    def isConnected(self):
        '''
            Cette méthode retourne vrai si la connexion à la BD est réussie
        '''
        if self.connection:
            return True
        else:
            return False

    def closeDB(self):
        '''
            Cette méthode ferme la connexion à la base de données
        '''
        if self.connection:
            self.connection.close()
            self.connection = None
            self.cursor = None

    def commit(self):
        ''' 
            Cette méthode sauvegarde les changements effectués dans la base de données 
        '''
        if self.connection:
            self.connection.commit()

    def getTable(self, table_name):
        '''
            Cette méthode retourne toutes les valeurs qui sont dans une table.

            @param (string) table_name: nom de la table existant dans la base de données
            @return: résultat de la requête,possiblement vide ou None si la table n'existe pas
        '''
        try:
            self.cursor.execute("SELECT * FROM " + table_name)
            return self.cursor.fetchall()
        except Error:
            return None

    def tableExists(self, table_name):
        '''
            Cette méthode retourne vrai si la table existe

            @param (string) table_name: nom de la table
            @return: Vrai si la table existe dans la BD
        '''
        exists = False
        query = "SELECT TRUE FROM sqlite_master WHERE tbl_name = (?)"
        try:
            self.cursor.execute(query, (table_name,))
            if self.cursor.fetchone() is not None:
                exists = True
        except Error:
            pass
        finally:
            return exists

    def createTable(self, table_name, column_names, column_types):
        '''
            Cette méthode crée une table si elle n'existe pas.

            @param (string, tuple, tuple) 
            @param table_name: nom de la table 
            @param column_names: noms des colonnes
            @param column_types: le type de colonnes
        '''
        query = 'CREATE TABLE ' + table_name + ' ( '
        # for col_name, col_type in column_names, column_types:
        #     query += col_name + ' ' + col_type + ','
        for i in range(len(column_names)):
            query += column_names[i] + ' ' + column_types[i] + ','
        query = query[:-1]
        query += ');'
        self.cursor.execute(query)

    def getTableInfo(self, table_name):
        try:
            query = "SELECT sql FROM sqlite_master WHERE tbl_name = (?);"
            self.cursor.execute(query, (table_name,))
            return (self.cursor.fetchall()[0][0])
        except Error:
            return ""

    def dropTable(self, table_name):
        '''
            Cette méthode supprime une table; ne retourne aucune erreur si la table n'existe pas

            @param (string) table_name: nom de la table
        '''
        try:
            self.cursor.execute("DROP TABLE IF EXISTS " + table_name)
        except Error:
            pass

    def insert(self, data, column_name, table_name):
        '''
            Cete métode insère des données dans une table existant

            @param (list of tuple) data: données à insérer
            @param (list of string) column_name: liste de nom de colonne correspondant respectivement aux 
                                                éléments dans chaque tuple de @param data
            @param (string) table_name: nom de table existant dans la base de données
        '''
        if not isinstance(data[0], tuple):
            data = [(elem,) for elem in data]

        queryInsert = r"INSERT INTO " + table_name + r"(" + r",".join(column_name) + r") values(" + \
            r"?," * (len(data[0])-1) + r"?);"

        self.cursor.executemany(queryInsert, data)

    def execute(self, query):
        '''
            Cette méthode permet d'exécuter des requêtes plus complexes

            @param query: requête à exécuter
            @Exception: lance une exception si la requête échoue
        '''
        self.cursor.execute(query)
        return self.cursor.fetchall()

    def clearAllTables(self):
        '''
            Cette méthode efface toutes les tables de la base de données
        '''
        try:
            self.cursor.execute("SELECT tbl_name FROM sqlite_master;")
            list_table_name = self.cursor.fetchall()
            for tup in list_table_name:
                self.cursor.execute("DROP TABLE IF EXISTS " + tup[0])
        except Error:
            pass
