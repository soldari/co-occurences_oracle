import numpy as np
from operator import itemgetter


class Synonyms:
    def __init__(self):
        self.wordMatrix = None
        self.synScores = None
        self.word = None
        self.nbSyn = None

    def reset(self, word, nbSyn):
        self.synScores = []
        self.word = word
        self.nbSyn = nbSyn

    def calculate(self, word, nbSyn, function, wordMatrix):
        self.wordMatrix = wordMatrix
        self.reset(word, nbSyn)
        self.function = function
        if word in wordMatrix.dict:
            # scalar
            if function == 0:
                self.scores(np.dot)
            # least squares
            elif function == 1:
                self.scores(np.subtract, np.square, np.sum)
            # city-block
            elif function == 2:
                self.scores(np.subtract, np.abs, np.sum)
            else:
                return -1
            self.sortSyn()
            return self.synScores[:self.nbSyn]
        else:
            not_found = [(self.word, 'pas dans le dictionnaire')]
            return not_found

    def sortSyn(self):
        self.synScores.sort(key=itemgetter(1), reverse=self.function == 0)

    def scores(self, f1, f2=None, f3=None):
        index = self.wordMatrix.dict[self.word]
        indexRow = self.wordMatrix.content[index]
        for word, index in self.wordMatrix.dict.items():
            if word not in self.wordMatrix.stopwords and word != self.word:
                score = f1(indexRow, self.wordMatrix.content[index])
                if f2:
                    score = f2(score)
                if f3:
                    score = f3(score)
                self.synScores.append((word, score))
