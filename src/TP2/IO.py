

class IO:
    introtext = '''
Entrez un mot m le nombre de 'synonymes' que vous voullez, et la méthode de calcul,
i.e. produit scalaire : 0, least-squares: 1, city-block:2

Tapez q pour quitter.

'''
    saved_success = 'les mots du fichier situe a {} ont été rajoutés avec succes à la base de donnée dans la table de taille {} avec l\'encodage {}'
    saved_fail = 'le fichier {} n\'a pas pu etre traité'

    eraseDB = 'Voulez-vous éffacer les tables enregistrées dans la base de données ? (O/N)'
    erased = 'Les tables de la base de donnée ont été éffacés'
    data_not_in_DB = 'il n\'y a pas de donnés pour la taille de fenêtre entrée'

    is_duplicate = 'le fichier {} a déjà été traité pour la taille de fenêtre {}'
    is_encoding_difference = 'l\'encodage de la fenêtre de taille {} est : {} et non pas {}'

    @staticmethod
    def showResults(results):
        print()
        for word, score in results:
            print(f"{word} --> {score}")

    @staticmethod
    def readInput():
        return input(IO.introtext).lower()

    @staticmethod
    def savedInDB(args):
        if args['success']:
            print(IO.saved_success.format(args['file_path'],
                                          args['window_size'], args['encoding']))
        else:
            print(IO.saved_fail.format(args['file_path']))

    @staticmethod
    def eraseDB():
        answer = input(IO.eraseDB).lower()
        return answer == 'o'
