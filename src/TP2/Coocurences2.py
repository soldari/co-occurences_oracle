from IO import *
from Parser import *
from Synonyms import *
from DataManager import *
from Connection import *


def training(args, c):
    dm = DataManager(c, args)
    dm.setup()
    IO.savedInDB(dm.save())


def search(args, c):
    dm = DataManager(c, args)
    dm.setup()
    if dm.tableSizeInDB():
        s = Synonyms()
        answer = IO.readInput()
        while(answer != 'q'):
            try:
                answer = answer.split()
                IO.showResults(s.calculate(
                    answer[0], int(answer[1]), int(answer[2]), dm.word_matrix))
            except Exception as e:
                raise e
            answer = IO.readInput()
    else:
        IO.dataNotInDB()


def erase(args, c):
    if IO.eraseDB():
        c.clearAllTables()
        print(IO.erased)


locals = locals()


def main():
    p = Parser()
    c = Connection('tp2')
    function, args = p.validateAnswers()
    locals[function](args, c)
    c.closeDB()


if __name__ == '__main__':
    quit(main())
