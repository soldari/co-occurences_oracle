import sys
import os

# print(sys.path)
relativePath = "..\\"
originalFilePath = os.path.realpath(os.path.join(os.path.dirname(__file__), relativePath))
# print(originalFilePath)
sys.path.append(originalFilePath)
# print(sys.path)


from Connection import *


# Test
if __name__ == "__main__":

    # Création du dossier db\
    print("Création du dossier db")
    try:
        os.mkdir(os.path.join(originalFilePath, "db"))
        print("dossier créé")
    except FileExistsError:
        print("dossier existe déjà")

    # chemin pour la BD
    dbpath = os.path.join(originalFilePath, r"db\dbtp2.bd")
    print("Chemin vers le BD")
    print(dbpath)

    # Création de l'objet connection
    print("Connexion à la DB")
    c = Connection(dbpath)
    
    # Test #1
    queryTable =''' CREATE TABLE IF NOT EXISTS projects (
                        id integer PRIMARY KEY,
                        name text NOT NULL
                    );
                '''
    data= [(3,"bonjour"), (5,"bye"), (6,"fun")]
    colNames = ["id","name"]
    tblName = r"projects"
    
    print("Création de la table " + tblName)
    c.createTable(queryTable)
    print("Description de la table " + tblName)
    print(c.getTableInfo(tblName))
    print(r"table projects existe => ", c.tableExists(r"projects"))
    print("Insertion des données dans la table " + tblName)
    c.insert(data,colNames,r"projects")
    print(r"getTable projects => ", c.getTable(r"projects"))

    # BD non existante
    print(r"getTable car(non existante) => ", c.getTable(r"car"))

    # Test #2
    queryTable2 =   ''' CREATE TABLE IF NOT EXISTS essai( 
                            nombre INTEGER
                        ); 
                    '''
    data2 = [(5), (180),(17998)]
    colNames2 = ["nombre"]
    tblName2 = "essai"

    print("Création de la table " + tblName2)
    c.createTable(queryTable2)
    print("Description de la table " + tblName2)
    print(c.getTableInfo(tblName2))
    print(r"table essai existe => ", c.tableExists(tblName2))
    print("Insertion des données dans la table " + tblName2)
    c.insert(data2,colNames2,tblName2)
    print(r"getTable essai => ", c.getTable(tblName2))
    
    print("Suppresion de la table " + tblName2)
    c.dropTable(tblName2)
    print(r"table essai existe => ", c.tableExists(tblName2))
    print(r"getTable essai => ", c.getTable(tblName2))

    print("Suppression de toutes les tables")
    c.clearAllTables()
    print(r"table projects existe => ", c.tableExists(tblName))
    print(r"table essai existe => ", c.tableExists(tblName2))


