from WordMatrix import *
from Connection import *
from IO import *


class DataManager:
    '''
            Classe servant à gerer les information entre la matrice de de mots et la base de données.
            @param (connection, args) 
                connection: objet Connection vers une base de données
                args: retour de la fonction validateAnswers de l'objet Parser
            @instance_variable (object) connection: instance de la classe Connection
            @instance_variable (object) word_matrix: objet de la classe WordMatrix initialisé avec le parametre 'args'
            @instance_variable (int) size: taille de la fenêtre de recherche
            @instance_variable (bool) training: est vrai si le mode choisit est de type 'entrainement'
    '''

    def __init__(self, connection: Connection, args):
        self.connection = connection
        self.training = args.training
        self.window_size = args.size
        self.path = args.chemin
        self.encoding = args.enc
        self.word_matrix = WordMatrix(
            self.window_size, self.encoding, self.path)

    def tableSizeInDB(self):
        table_name = 'dict' + str(self.window_size)
        return self.connection.tableExists(table_name)

    def fetchDict(self):
        '''
            Cette méthode va chercher dans la BD, si elles existent, les données du dictionaire pour une taille de fenêtre donnée.
        '''
        table_name = 'dict' + str(self.window_size)
        if self.connection.tableExists(table_name):
            key_index_list = self.connection.getTable(table_name)
            self.word_matrix.fillDictFromDB(key_index_list)

    def storeDict(self):
        '''
            Cette méthode va storer dans la BD les données du dictionaire pour une taille de fenêtre donnée.
        '''
        stored = False
        data = self.word_matrix.dict.items()
        if len(data) != 0:
            table_name = 'dict_' + str(self.window_size)
            column_names = ('key', 'keyindex')
            column_types = ('text', 'int')
            self.resetTable(table_name, column_names, column_types)
            self.connection.insert(list(data), column_names, table_name)
            stored = True
        return stored

    def fetchCooc(self):
        '''
            Cette méthode va chercher dans la BD, si elles existent, les données des coocurences pour une taille de fenêtre donnée.
        '''
        table_name = 'cooc_' + str(self.window_size)
        if self.connection.tableExists(table_name):
            value_list = self.connection.getTable(table_name)
            self.word_matrix.fillFromValueList(value_list)

    def storeCooc(self):
        '''
            Cette méthode va storer dans la BD les données des coocurences pour une taille de fenêtre donnée.
        '''
        stored = False
        data = self.word_matrix.nonNullValues()
        if len(data) != 0:
            table_name = 'cooc'+str(self.window_size)
            column_names = ('i', 'j', 'value')
            column_types = ('int', 'int', 'int')
            self.resetTable(table_name, column_names, column_types)
            self.connection.insert(list(data), column_names, table_name)
            stored = True
        return stored

    def resetTable(self, table_name, column_names, column_types):
        '''
            Cette méthode efface et cree une table dans la BD.

            @param (string, tuple, tuple) 
                table_name: nom de la table
                column_names: nom des colonnes de la table
                column_types: type des colonnes de la table
        '''
        if self.connection.tableExists(table_name):
            self.connection.dropTable(table_name)
        self.connection.createTable(table_name, column_names, column_types)

    def validateContinue(self):
        table_name = 'info'
        isEncodingDifference = False
        isDuplucate = False
        window_size = None
        file_path = None
        encoding = None
        if self.connection.tableExists(table_name):
            value_list = self.connection.getTable(table_name)
            for window_size, file_path, encoding in value_list:
                if window_size == self.window_size:
                    if encoding != self.encoding:
                        isEncodingDifference = True
                    if file_path == self.path:
                        isDuplucate = True
        if isDuplucate:
            print(IO.is_duplicate.format(file_path, window_size))
        if isEncodingDifference:
            print(IO.is_encoding_difference.format(
                window_size, encoding, self.encoding))
        return isDuplucate+isEncodingDifference == 0

    def setup(self):
        '''
            Cette méthode va chercher les données de la BD et les insere das la matrice 
            de coocurences et le dictionnaire. 
            Si le mode d'entrainement est selectionné, elle rajoute les mots du nouveau 
            texte dans la matrice de coocurences et le dictionnaire.
        '''
        if self.validateContinue():
            self.fetchDict()
            if self.training:
                self.word_matrix.addWordsFromText()
                self.word_matrix.fillDict()
            self.word_matrix.createWordMatrix()
            self.fetchCooc()
            if self.training:
                self.word_matrix.fillWordMatrix()

    def addInfo(self):
        pass
        '''
            Cette méthode enregistre la taille de fenêtre, le nom du texte et l'encodage 
            du texte ajouté dans la bd
        pseudo code:
        '''
        table_name = 'info'
        column_names = ('window_size', 'file_path', 'encoding')
        column_types = ('int', 'text', 'text')
        if not self.connection.tableExists(table_name):
            self.connection.createTable(table_name, column_names, column_types)
        data = [(self.window_size, self.path, self.encoding)]
        self.connection.insert(list(data), column_names, table_name)

    def save(self):
        '''
            Cette méthode insere le dictionnaire et les coocurences dans la BD et enregistre les changements.
        '''
        saved = False
        try:
            saved = self.storeDict() + self.storeCooc() != 0
            if saved:
                self.addInfo()
                self.connection.commit()
        except Exception as e:
            raise e
        statement = {'success': saved,
                     'file_path': self.path,
                     'encoding': self.encoding,
                     'window_size': self.window_size
                     }
        return statement
