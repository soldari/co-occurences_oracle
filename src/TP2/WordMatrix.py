from Util import *
import numpy as np


class WordMatrix:
    '''
        La classe WordMatrix store une matrice de coocurences et son dictionnaire associé.

        @param (args) args: retour de la fonction validateAnswers de l'objet Parser
        @instance_variable (list) words : liste de tous les mots compris dans les 
            textes à integrer
        @instance_variable (matrix) content: matrice de la dimention dudictionnaire ^2 
            dans laquelle sont stockées les coocurences
        @instance_variable (dict) dict: dictionnaire dont la clé est un mot et dont la 
            valeur est l'index du mot dans la matrice
        @instance_variable (dict) stopwords: dictionnaire des mots d'arrêt
        @instance_variable (int) size: taille de la fenêtre de recherche
        @instance_variable (string) encoding: type d'encodage
        @instance_variable (string) text_Path: le chemin du texte d'entrainement
    '''

    def __init__(self, window_size, encoding, path):
        self.words = []
        self.content = None
        self.dict = {}
        self.stopwords = {}
        self.windowSize = window_size
        self.encoding = encoding
        self.textPath = path

    def fillDict(self):
        '''
            Traite les mots contenus dans 'words' dans le dictionnaire 'dict'
        '''
        Util.fillDictionary(self.words, self.dict)

    def addWordsFromText(self):
        '''
            Met les mots du texte associé au chemin dans 'words'
        '''
        if self.textPath != None:
            self.words += Util.text2words(
                Util.read(self.textPath, self.encoding))

    def createWordMatrix(self):
        '''
            Cette méthode cree une matrice de zéros de la taille du dictionnaire ^2
        '''
        self.content = np.zeros((len(self.dict), len(self.dict)))

    def addWordInMatrix(self, word1, word2):
        '''
            Cette méthode incrémente une coocurence située à 
            content[l'index du 1er mot][l'index du 2eme mot] de 1
        '''
        self.content[self.dict[self.words[word1]]
                     ][self.dict[self.words[word2]]] += 1

    def fillWordMatrix(self):
        '''
            Cette méthode remplit la matrice avec les mots contenus dans 'words'
        '''
        half_span = int(self.windowSize/2)
        text_length = len(self.words)
        # for first words
        for i in range(0, half_span):
            for j in range(0, i):
                self.addWordInMatrix(i, i-j)
            for j in range(1, half_span+1):
                self.addWordInMatrix(i, i+j)
        # for all other words
        for i in range(half_span, text_length - half_span):
            for j in range(0, half_span):
                self.addWordInMatrix(i, i-j)
            for j in range(1, half_span+1):
                self.addWordInMatrix(i, i+j)
        # for end words
        for i in range(text_length - half_span, text_length):
            for j in range(0, half_span):
                self.addWordInMatrix(i, i-j)
            for j in range(1, text_length - i):
                self.addWordInMatrix(i, i+j)

    def nonNullValues(self):
        ''' 
            Cette méthode retourne les valeurs non-nulles de la matrice de maniere symétrique:
            c-a-d: la moitié du bas de la diagonale symétrique
        '''
        values = []

        for i in range(0, len(self.dict)):
            for j in range(0, i+1):
                if self.content[i][j] != 0:
                    values.append((i, j, int(self.content[i][j])))
        return values

    def fillFromValueList(self, value_list):
        ''' 
            Cette méthode insere dans la matrice de coocurences les valeurs 
            non-nulles de la BD de maniere symétrique
            Doit etre execute avant les entrees dans la matrice a partir du texte.
        '''
        for i, j, value in value_list:
            self.content[i][j] = self.content[j][i] = value

    def fillDictFromDB(self, key_index_list):
        ''' 
            Cette méthode remplit le dictionnaire a partir d'une BD.
            Doit etre execute avant les entrees dans le dictionnaire a partir du texte.
        '''
        for key, index in key_index_list:
            self.dict[key] = index

    def fillStopwordsFromDB(self, key_index_list):
        for key, index in key_index_list:
            self.stopwords[key] = index
