import re
from builtins import staticmethod


class Util:

    @staticmethod
    def read(path, enc):
        try:
            f = open(path, 'r', encoding=enc)
            text = f.read().lower()
            f.close()
            return text
        except Exception as e:
            raise e

    @staticmethod
    def text2words(text):
        words = re.findall('\w+', text)
        return words

    @staticmethod
    def fillDictionary(words, dict):
        for word in words:
            if word not in dict:
                dict[word] = len(dict)
