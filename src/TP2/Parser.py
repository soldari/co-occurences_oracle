import argparse


class Parser:

    def __init__(self):
        self.parser = argparse.ArgumentParser(description='Coocurence parser')

        self.parser.add_argument('-r', '--search', action="store_true",
                                 help='pour selectionner le mode recherche. il faut aussi specifier la taille')
        self.parser.add_argument('-e', '--training', action="store_true",
                                 help='pour selectionner le mode entrainement, il faut aussi specifier la taille, le chemin et l\'encodage')
        self.parser.add_argument(
            '--erase', action="store_true", help='pour effacer la BD')
        self.parser.add_argument('--stopwords', action="store_true",
                                 help='pour rajouter des stopwords, il faut aussi specifier le chemin et l\'encodage')
        self.parser.add_argument('-t', '--size', action="store",
                                 help='taille de la fenêtre', type=int)
        self.parser.add_argument(
            '--enc', action="store", help='type d\'encodage')
        self.parser.add_argument(
            '--chemin', action="store", help='chemin du fichier d\'entrainement ou stopwords')

    def validateAnswers(self):
        args = self.parser.parse_args()

        if (args.training + args.search + args.stopwords + args.erase != 1):
            self.parser.error(
                'Enter -e, -r, --erase or --stopwords ; --training --search, --erase and --stopwords are mutually exclusive and required')
        else:
            if (args.training):
                if (args.enc != None and args.chemin != None and args.size != None):
                    return ('training', args)
                else:
                    self.parser.error(
                        '-t --chemin and --enc are required for -e / --training')
            elif args.search:
                if (args.size != None):
                    return ('search', args)
                else:
                    self.parser.error(
                        '-t is required for -r / --search')
            elif args.stopwords:
                if (args.enc != None and args.chemin != None and args.size != None):
                    return ('stopwords', args)
                else:
                    self.parser.error(
                        '--chemin and --enc are required for --stopwords')
            else:
                return ('erase', args)


def test():
    p = Parser()
    print(p.validateAnswers())


if __name__ == '__main__':
    quit(test())
