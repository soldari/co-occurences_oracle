from sys import argv
import os
import re
import numpy as np
import traceback
import time
from operator import itemgetter
from builtins import staticmethod


introtext = '''
Entrez un mot m le nombre de 'synonymes' que vous voullez, et la méthode de calcul,
i.e. produit scalaire : 0, least-squares: 1, city-block:2

Tapez q pour quitter.

'''
stopwordspath = './stopwords.txt'


class Util:

    @staticmethod
    def read(path, enc):
        try:
            f = open(path, 'r', encoding=enc)
            text = f.read().lower()
            f.close()
            return text
        except Exception as e:
            raise e

    @staticmethod
    def text2words(text):
        words = re.findall('[-\w]+', text)
        return words

    @staticmethod
    def fillDictionary(words, dict):
        for word in words:
            if word not in dict:
                dict[word] = len(dict)


class WordMatrix:
    def __init__(self, argv):
        self.words = []
        self.wordMatrix = None
        self.dict = {}
        self.stopwords = {}
        self.windowSize = int(argv[1])
        self.encoding = argv[2]
        self.textPaths = argv[3:]

    def addWordsFromTexts(self):
        for textPath in self.textPaths:
            self.words += Util.text2words(Util.read(textPath, self.encoding))

    def createWordMatrix(self):
        self.wordMatrix = np.zeros((len(self.dict), len(self.dict)))

    def addWordInMatrix(self, index1, index2):
        self.wordMatrix[self.dict[self.words[index1]]
                                ][self.dict[self.words[index2]]] += 1

    def fillWordMatrix(self):
        half_span = int(self.windowSize/2)
        text_length=len(self.words)
        # for first words
        for i in range(0, half_span):
            for j in range(0,i):
                self.addWordInMatrix(i, i-j)
            for j in range(1, half_span+1):
                self.addWordInMatrix(i, i+j)
        # for all other words
        for i in range(half_span, text_length - half_span):
            for j in range(0, half_span):
                self.addWordInMatrix(i, i-j)
            for j in range(1, half_span+1):
                self.addWordInMatrix(i, i+j)
        # for end words
        for i in range(text_length - half_span, text_length):
            for j in range(0, half_span):
                self.addWordInMatrix(i, i-j)
            for j in range(1, text_length - i):
                self.addWordInMatrix(i, i+j)


    def setup(self):
        t = time.time()
        self.addWordsFromTexts()
        Util.fillDictionary(self.words, self.dict)
        Util.fillDictionary(Util.text2words(
            Util.read(stopwordspath,self.encoding)), self.stopwords)
        self.createWordMatrix()
        self.fillWordMatrix()
        duree = time.time() - t
        print(f'Entrainement en {duree} secondes.')


class Coocurences:
    def __init__(self):
        self.wordMatrix = None
        self.synScores = None
        self.word = None
        self.nbSyn = None

    def reset(self, word, nbSyn):
        self.synScores = []
        self.word = word
        self.nbSyn = nbSyn

    def calculate(self, word, nbSyn, function, wordMatrix):
        self.wordMatrix = wordMatrix
        self.reset(word, nbSyn)
        self.function = function
        # scalar
        if function == 0:
            self.scores(np.dot)
        # least squares
        elif function == 1:
            self.scores(np.subtract, np.square, np.sum)
        # city-block
        elif function ==2:
            self.scores(np.subtract, np.abs, np.sum)
        else:
            return -1
        self.sortSyn()
        return self.synScores[:self.nbSyn]

    def sortSyn(self):
        self.synScores.sort(key=itemgetter(1), reverse=self.function == 0)

    def scores(self, f1, f2=None, f3=None):
        index = self.wordMatrix.dict[self.word]
        indexRow = self.wordMatrix.wordMatrix[index]
        for word, index in self.wordMatrix.dict.items():
            if word not in self.wordMatrix.stopwords and word != self.word:
                score = f1(indexRow, self.wordMatrix.wordMatrix[index])
                if f2:
                    score = f2(score)
                if f3:
                    score =f3(score)
                self.synScores.append((word, score))


class IO:
    @staticmethod
    def showResults(results):
        print()
        for word, score in results:
            print(f"{word} --> {score}")

    @staticmethod
    def readInput():
        return input(introtext).lower()


def main():

    wm = WordMatrix(argv)
    wm.setup()
    c = Coocurences()
    answer = IO.readInput()
    while(answer != 'q'):
        try:
            answer = answer.split()
            IO.showResults(c.calculate(answer[0], int(answer[1]), int(answer[2]), wm))
        except Exception as e:
            raise e
        answer = IO.readInput()


if __name__ == '__main__':
    quit(main())

# Cooccurrences.py 5 utf-8 ./textes/GerminalUTF8.txt ./textes/LesTroisMousquetairesUTF8.txt ./textes/LeVentreDeParisUTF8.txt
# Cooccurrences.py 5 utf-8 ./textes/test.txt
